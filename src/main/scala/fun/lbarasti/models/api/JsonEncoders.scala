package fun.lbarasti.models.api

import io.circe.{Decoder, Encoder}

object JsonEncoders {
  implicit val encoder: Encoder[HealthStatus.Value] = Encoder.enumEncoder(HealthStatus)
  implicit val decoder: Decoder[HealthStatus.Value] = Decoder.enumDecoder(HealthStatus)
}
