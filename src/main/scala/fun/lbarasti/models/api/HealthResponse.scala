package fun.lbarasti.models.api

object HealthStatus extends Enumeration {
  type HealthStatus = Value
  val Green, Amber, Red = Value
}

case class HealthResponse(status: HealthStatus.HealthStatus, externalServices: Map[String, String])
