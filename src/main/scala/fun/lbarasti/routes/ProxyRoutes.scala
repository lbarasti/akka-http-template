package fun.lbarasti.routes

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, StatusCodes}
import akka.http.scaladsl.server.{Directives, Route}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport

import scala.concurrent.Future
import scala.util.{Failure, Success}

class ProxyRoutes(implicit system: ActorSystem) extends Directives with FailFastCirceSupport {
  def routes: Route = (path("proxy" / Segment) & get) { url =>
    val responseFuture: Future[HttpResponse] =
      Http().singleRequest(HttpRequest(uri = url))

    onComplete(responseFuture) {
      case Success(response) => complete(HttpResponse(StatusCodes.OK, entity = response.entity))
      case Failure(error) => complete(ProxyError(error.getMessage))
    }
  }
}

case class ProxyError(message: String) extends Throwable