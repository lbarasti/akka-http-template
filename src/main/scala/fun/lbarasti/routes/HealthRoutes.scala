package fun.lbarasti.routes

import akka.actor.ActorSystem
import akka.http.scaladsl.server.{Directives, Route}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import fun.lbarasti.models.api.{HealthResponse, HealthStatus}
import fun.lbarasti.models.api.JsonEncoders._
import fun.lbarasti.tracing.KamonKeys._
import kamon.Kamon
import org.slf4j.{LoggerFactory, MDC}

class HealthRoutes(implicit actorSystem: ActorSystem) extends Directives with FailFastCirceSupport {
  val log = LoggerFactory.getLogger(getClass)

  def routes: Route = get {
    path("health") {
      MDC.put(requestIdKey.name, Kamon.currentContext().get(requestIdKey).getOrElse(""))
      log.debug("Responding to healthcheck...")
      MDC.remove(requestIdKey.name)
      complete(HealthResponse(HealthStatus.Green, Map.empty))
    }
  }
}
