package fun.lbarasti.tracing

import kamon.context.Key

object KamonKeys {
  val hostKey = Key.broadcastString("host")
  val requestIdKey = Key.broadcastString("request-id")
}
