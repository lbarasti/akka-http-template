package fun.lbarasti.tracing

import akka.http.scaladsl.server.{Directive0, Directives}
import kamon.Kamon
import kamon.context.Context
import KamonKeys._

object TracingDirectives extends Directives {
  def propagateContext(context: Context): Directive0 =
    mapInnerRoute { route =>
      optionalHeaderValueByName("request-id") { requestId =>
        ctx => {
          Kamon.withContext(context.withKey(requestIdKey, requestId)) {
            route(ctx)
          }
        }
      }
    }
}
