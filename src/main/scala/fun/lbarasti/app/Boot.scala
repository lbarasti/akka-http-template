package fun.lbarasti.app

import akka.actor.ActorSystem
import akka.http.scaladsl.server.{HttpApp, Route}
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import fun.lbarasti.routes.{HealthRoutes, ProxyRoutes}
import fun.lbarasti.tracing.KamonKeys._
import fun.lbarasti.tracing.TracingDirectives
import kamon.Kamon
import kamon.context.Context
import kamon.prometheus.PrometheusReporter

object Boot extends HttpApp with App {
  val config = ConfigFactory.load()
  implicit val actorSystem = ActorSystem("akka-http-template")
  implicit val materializer = ActorMaterializer.create(actorSystem)
  Kamon.addReporter(new PrometheusReporter())

  val context = Context().withKey(hostKey, Some(config.getString("server.host")))

  override protected def routes: Route = TracingDirectives.propagateContext(context) {
    new HealthRoutes().routes ~
      new ProxyRoutes().routes
  }

  startServer("0.0.0.0", config.getInt("server.port"))
}
