package fun.lbarasti.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import org.scalatest.{Matchers, WordSpec}
import fun.lbarasti.models.api.{HealthResponse, HealthStatus}
import fun.lbarasti.models.api.JsonEncoders._

class HealthRouteTest extends WordSpec with Matchers with ScalatestRouteTest with FailFastCirceSupport {
  "The /health route" should {
    "Return OK on GET" in {
      Get("/health") ~> new HealthRoutes().routes ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[HealthResponse] shouldEqual HealthResponse(HealthStatus.Green, Map.empty)
      }
    }
  }
}
